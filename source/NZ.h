#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <random>
#include <set>
#include <stdio.h>
#include <string>

#include "NZ_util.h"

// This is a C++ implementation of Newman-Ziff algorithm.
// Please cite: 
// Newman, M. E. J., & Ziff, R. M. (2001). Fast Monte Carlo algorithm for site or bond percolation. 
// In Physical Review E (Vol. 64, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physreve.64.016706.
//
// You must create your own construct_network function to construct a specific network. 

namespace CPercolate {
   void swap_elm(
         std::vector<std::array<long int,2>> &vec,
         long int ind0, long int ind1
         )
   {
      std::array<long int,2> temp;
      temp=vec[ind0];
      vec[ind1]=vec[ind0];
      vec[ind0]=temp;
   }

   bool find(std::vector<std::vector<long int>> vecs, long int num)
   {
      for(auto vec=vecs.begin(); vec != vecs.end(); vec++)
      {
         for(auto n =(*vec).begin(); n != (*vec).end(); n++)
         {
            if ((*n)==num) return true;
         }
      }
      return false;
   }

   std::pair<bool,long int> find(std::vector<long int> vec, long int num)
   {
      long int ind=0;
      for(auto n =vec.begin(); n != vec.end(); n++)
      {
         if ((*n)==num) return std::make_pair(true,ind);
         ind += 1;
      }

      return std::make_pair(false,ind);
   }

   std::array<long int,2> find_next(
      const std::array<long int,2> edge_now,
      const std::vector<std::array<long int,2>> &edges_next)
   {
      std::array<long int,2> out={-1,-1};
      bool found0=false;
      bool found1=false;

      long int count=0;
      for( auto it_next=edges_next.begin() ; it_next != edges_next.end(); it_next++)
      {
         if( ( edge_now[0] == (*it_next)[0]  ) ||  ( edge_now[0] == (*it_next)[1] ) )
         {
            if (! found0) 
            {
               out[0] = count;
               found0=true;
            }
         }

         if( ( edge_now[1] == (*it_next)[0]  ) ||  ( edge_now[1] == (*it_next)[1] ) )
         {
            if( ! found1)
            {
               out[1] = count;
               found1=true;
            }
         }

         if (found0 && found1 ) break ;

         count+=1;
      }

      if ((! found0) && (! found1) )
      {
         std::cout << "not found!" << std::endl;
         std::cout << edge_now[0] << "," << edge_now[1] << std::endl;
         throw 2;
      }
      else if ((! found0) || (! found1) )
      {
         std::cout << "find_next: somtehing went wrong" << std::endl;
         std::cout << edge_now[0] << "," << edge_now[1] << std::endl;
         throw 2;
      }

      return out;
   }

   long int find_root(
         std::vector<long int> &node_labels,
         long int num
         )
   {
      while( node_labels[num] != num )
      {
         node_labels[num]=node_labels[node_labels[num]];
         num=node_labels[num];
      }

      return num;
   }

   bool check_spanning(
         std::vector<long int> &sp_labels,
         long int num_nodes,
         long int num_aux_nodes0,
         long int num_aux_nodes1
         )
   {
      for(long int aux_node_ind0=0;aux_node_ind0<num_aux_nodes0;aux_node_ind0++)
      {
         for(long int aux_node_ind1=0;aux_node_ind1<num_aux_nodes1;aux_node_ind1++)
         {
            if (find_root(sp_labels,num_nodes+aux_node_ind0)==find_root(sp_labels,num_nodes+num_aux_nodes0+aux_node_ind1)) 
            {
               return true;
            }
         }
      }
      return false;
   }

   void quick_union(
         std::vector<long int> &node_labels,
         std::vector<long int> &cl_sizes,
         std::array<long int,2> edge
         )
   {
      bool verbose=false;
      long int root0=find_root(node_labels,edge[0]);
      long int root1=find_root(node_labels,edge[1]);

      if ( ! (root0 == root1) )
      {
         if(verbose) std::cout << "merged " << root0 << " and " << root1 << std::endl;
         if ( cl_sizes[root0] > cl_sizes[root1] )
         {

            node_labels[root1]=root0;
            cl_sizes[root0] += cl_sizes[root1];
         }
         else
         {
            node_labels[root0]=root1;
            cl_sizes[root1] += cl_sizes[root0];
         }
      }
   }

   void append_cluster(
         std::vector<long int> &node_labels,
         std::vector<long int> &cl_sizes,
         std::array<long int,2> edge,
         std::vector<double> &moments,
         long int &max_cluster_root,
         long int &max_cluster_size
         )
   {
      bool verbose=false;
      long int root0=find_root(node_labels,edge[0]);
      long int root1=find_root(node_labels,edge[1]);

      if ( ! (root0 == root1) )
      {
         if(verbose) std::cout << "merged " << root0 << " and " << root1 << std::endl;

         for(int m_exp=0; m_exp<moments.size();m_exp++)
         {
            moments[m_exp] = moments[m_exp] 
               - std::pow(double(cl_sizes[root0]),double(m_exp+1.) )
               - std::pow(double(cl_sizes[root1]),double(m_exp+1.))
               + std::pow(double(cl_sizes[root0]+cl_sizes[root1]),double(m_exp+1));
         }

         if ( cl_sizes[root0] > cl_sizes[root1] )
         {
            node_labels[root1]=root0;
            cl_sizes[root0] += cl_sizes[root1];

            if (cl_sizes[root0] > max_cluster_size)
            {
               max_cluster_root=root0;
               max_cluster_size=cl_sizes[root0];
            }
         }
         else
         {
            node_labels[root0]=root1;
            cl_sizes[root1] += cl_sizes[root0];

            if (cl_sizes[root1] > max_cluster_size)
            {
               max_cluster_root=root1;
               max_cluster_size=cl_sizes[root1];
            }
         }
         return;
      }
   }

   class CPercolate
   { 
      public :
         CPercolate(){} ;
         CPercolate(long int N_in, long int max_t_in)
            : N(N_in), max_t(max_t_in) {};

         void set_verbose(bool verbose_in){verbose=verbose_in;}
         void construct_network_NN();
         void construct_network_PWR2();
         void construct_network_PWR2_depth(int depth);
         void construct_network_AA();

         void construct_network_Square();
         void NZ(unsigned int seed_in);

         void erase_obs()
         { 
            max_cluster_sizes.erase(
               max_cluster_sizes.begin(),max_cluster_sizes.end());
            spannings.erase(
               spannings.begin(),spannings.end());
            moments_list.erase(moments_list.begin(),moments_list.end());
            chi.erase(chi.begin(),chi.end());
         }

         void output_chi(std::string destdir="./")
         {
            std::string fname=destdir+"chi_"+std::to_string(N)+"_"+std::to_string(seed)+".bin";
            write_binary(fname,chi);
         }

         void output_obs(std::string destdir="./")
         {
            std::string fname;
            fname=destdir+"chi_"+std::to_string(N)+"_"+std::to_string(seed)+".bin";
            if (chi.size() != num_edges)
            {
               std::cout << "something went wrong" << std::endl;
               std::cout << "there are " << chi.size() << " data points" << std::endl;
               std::cout << "there are " << num_edges << " edges" << std::endl;
               throw 1;
            }
            write_binary(fname,chi);

            fname=destdir+"mcs_"+std::to_string(N)+"_"+std::to_string(seed)+".bin";
            write_binary(fname,max_cluster_sizes);

            fname=destdir+"sp_"+std::to_string(N)+"_"+std::to_string(seed)+".bin";
            write_binary(fname,spannings);

            for(int m=0;m<num_moments;m++)
            {
               std::vector<double> moments_out;
               fname=destdir+"mm_"+std::to_string(m+1)+"_"+std::to_string(N)+"_"+std::to_string(seed)+".bin";
               for(auto m_it=moments_list.begin(); m_it!=moments_list.end();m_it++)
               {
                  moments_out.push_back((*m_it)[m]);
               }
               write_binary(fname,moments_out);
            }
         }

      private :
         long int N=0;
         long int max_t=0;

         long int num_edges;
         long int num_nodes;
         long int num_aux_nodes0;
         long int num_aux_nodes1;

         int seed=0;

         std::vector<std::array<long int,2>> edges;
         std::vector<std::array<long int,2>> edges_orig;
         std::vector<long int> labels;
         std::vector<long int> labels_orig;
         std::vector<long int> cluster_sizes;

         std::vector<std::array<long int,2>> sp_edges;
         std::vector<long int> sp_labels;
         std::vector<long int> sp_labels_orig;
         std::vector<long int> sp_cluster_sizes;

         // Observables 
         int num_moments=3;
         std::vector<long int> max_cluster_sizes;
         std::vector<std::vector<double>> moments_list;
         std::vector<int> spannings;

         std::vector<double> chi; // obsolete

         bool constructed=false;
         bool verbose=false;
   }
   ;

   void CPercolate::NZ(unsigned int seed_in=0)
   {
      if( ! constructed )
      {
         std::cerr << "Lattice not constructed" << std::endl;
         std::exit(1);
      }

      std::cout << "seed=" << seed_in << std::endl;

      std::mt19937 mt(seed_in);
      seed=seed_in;

      bool spanning=false;
      long int max_cluster_root=0;
      long int max_cluster_size=1;

      std::vector<double> moments(num_moments,double(num_nodes));
      for(long int edge_ind=0; edge_ind<edges.size();edge_ind++)
      {
         std::uniform_int_distribution<> rand(edge_ind,edges.size()-1);
         long int randind = rand(mt) ;

         if (verbose) std::cout << randind << std::endl;

         // std::cout << "koko" << edges[randind][0] << "," << edges[randind][1] << std::endl;
         if (! spanning)
         {
            quick_union(sp_labels,sp_cluster_sizes,edges[randind]);
            spanning=(find_root(sp_labels,num_nodes) == find_root(sp_labels,num_nodes+1));
         }

         append_cluster(labels,cluster_sizes,edges[randind],moments,max_cluster_root,max_cluster_size);
         spannings.push_back(int(spanning));
         std::vector<double> moments_temp=moments;
         if (spanning)
         {
            for(int m_ind=0;m_ind<moments_temp.size();m_ind++) moments_temp[m_ind] -= std::pow(double(max_cluster_size),double(m_ind+1));
         }

         max_cluster_sizes.push_back(max_cluster_size);
         moments_list.push_back(moments_temp);

         double sus=moments_temp[1];
         // if (edge_ind%10000 == 0) 
         // std::cout<<"\rsus " << std::setw(10)  << spanning << std::setw(20)  << sus << std::flush;
         chi.push_back(sus);

         swap_elm(edges,edge_ind,randind);
      }

      cluster_sizes=std::vector<long int>(num_nodes,1);
      labels=labels_orig;
      edges=edges_orig;

      sp_cluster_sizes=std::vector<long int>(num_nodes+2,1);
      sp_labels=sp_labels_orig;

      for(auto sp_edge=sp_edges.begin(); sp_edge != sp_edges.end(); sp_edge++)
      {
         // std::cout << (*sp_edge)[0] << "," << (*sp_edge)[1] << std::endl;
         quick_union(sp_labels,sp_cluster_sizes,*sp_edge);
      }

      std::cout << "done" << std::endl;
   }

   void CPercolate::construct_network_NN()
   {
      if( constructed )
      {
         std::cerr << "Lattice already constructed" << std::endl;
         std::exit(1);
      }

      num_edges=2*(N/2)*(max_t);
      num_nodes=(N/2)*(max_t+1);

      num_aux_nodes0=N/2;
      num_aux_nodes1=N/2;

      edges.reserve(num_edges);
      labels.reserve(num_nodes);
      sp_labels.reserve(num_nodes+2);

      int dmax=1;
      int block_num_layers=2*dmax;

      long int num_edges_block=2*(block_num_layers)*(N/2);
      long int num_nodes_block=(N/2)*(block_num_layers+1);

      std::vector<std::array<long int,2>> edges_block;

      edges_block.reserve(num_edges_block);

      long int node_block_now=0;
      for( long int t_now=0;t_now<block_num_layers;t_now++)
      {
         std::vector<std::array<long int,2>> edges_now;
         std::vector<std::array<long int,2>> edges_next;

         unsigned int d=1;
         unsigned int edges_now_ind=0;

         for( long int site0=0; site0  < d ; site0 ++)
         {
            for(long int site=(site0+d*(t_now%2)); site<N; site+=2*d)
            {
               std::array<long int,2> edge_now;
               edge_now[0]=site;
               edge_now[1]=(edge_now[0]+d)%N;
               edges_now.push_back(edge_now);
               edges_now_ind+=1;
            }
         }

         d=1;
         unsigned int edges_next_ind=0;

         for( long int site0=0; site0  < d ; site0 ++)
         {
            for(long int site=(site0+d*((t_now+1)%2)); site<N; site+=2*d)
            {
               std::array<long int,2> edge_now;
               edge_now[0]=site;
               edge_now[1]=(edge_now[0]+d)%N;
               edges_next.push_back(edge_now);
               edges_next_ind+=1;
            }
         }

         if (edges_now.size() != N/2)
         {
            std::cerr << "Number of sites and number of edges don't match" << std::endl;
            std::cerr << "N/2=" << N/2 << std::endl;
            std::cerr << "Number of edges" << edges_now.size() << std::endl;
            std::exit(1);
         }

         for( auto it_now=edges_now.begin() ; it_now !=edges_now.end(); it_now++)
         {
            // std::cout << (*it_now)[0]  << "," << (*it_now)[1] << std::endl;
            //
            std::array<long int,2> connected_node;
            connected_node=find_next(*it_now,edges_next);
            connected_node[0]+= (t_now+1)*(N/2);
            connected_node[1]+= (t_now+1)*(N/2);

            // std::cout << node_block_now << "," << connected_node[0] << std::endl;
            // std::cout << node_block_now << "," << connected_node[1] << std::endl;

            edges_block.push_back( std::array<long int,2>{node_block_now, connected_node[0]} );
            edges_block.push_back( std::array<long int,2>{node_block_now, connected_node[1]} );
            node_block_now+=1;
         }
      }

      if (edges_block.size() != num_edges_block )
      {
         std::cerr << "number of edges per block don't match" << std::endl;
         std::cerr << "There supposed to be " << 2*(block_num_layers*N/2) << " edges" << std::endl;
         std::cerr << "Number of edges" << edges_block.size() << std::endl;
         std::exit(1);
      }

      for( long int t_now=0;t_now<max_t;t_now++)
      {
         long int offset_node = t_now/block_num_layers;
         long int offset_block = t_now%block_num_layers;

         for(long int ind=0; ind<2*(N/2); ind++)
         {
            std::array<long int,2> edge_now;
            edge_now=edges_block[ind+2*offset_block*(N/2)];
            edge_now[0]+= block_num_layers*offset_node*(N/2);
            edge_now[1]+= block_num_layers*offset_node*(N/2);
            edges.push_back(edge_now);
         }
      }

      if(edges.size() != num_edges )
      {
         std::cerr << "number of total edges don't match" << std::endl;
         std::cerr << "There supposed to be " << num_edges << " edges" << std::endl;
         std::cerr << "Number of edges " << edges.size() << std::endl;
         std::exit(1);
      }

      for(long int node=0;node<(num_nodes); node++)
      {
         labels.push_back(node);
      }

      edges_orig=edges;
      labels_orig=labels;
      cluster_sizes=std::vector<long int>(num_nodes,1);

      sp_labels=labels;
      for(long int node=0;node<2;node++)
      {
         sp_labels.push_back(node);
      }

      sp_labels[num_nodes+0]=num_nodes+0;
      for(long int node=0;node<num_aux_nodes0;node++)
      {
         sp_edges.push_back(std::array<long int,2> {node,num_nodes+0});
      }

      sp_labels[num_nodes+1]=num_nodes+1;
      sp_labels_orig=sp_labels;
      sp_cluster_sizes=std::vector<long int>(num_nodes+2,1);

      for(long int node=0;node<num_aux_nodes1;node++)
      {
         sp_edges.push_back(std::array<long int,2> {num_nodes-(num_aux_nodes1)+node,num_nodes+1});
      }

      for(auto sp_edge=sp_edges.begin(); sp_edge != sp_edges.end(); sp_edge++)
      {
         quick_union(sp_labels,sp_cluster_sizes,*sp_edge);
      }

      std::cout <<"Lattice constructed" << std::endl;
      constructed=true;
   }

   void CPercolate::construct_network_Square()
   {
      if( constructed )
      {
         std::cerr << "Lattice already constructed" << std::endl;
         std::exit(1);
      }

      num_edges=2*N*(max_t);
      num_nodes=N*(max_t+1);

      num_aux_nodes0=N;
      num_aux_nodes1=N;

      edges.reserve(num_edges);
      labels.reserve(num_nodes);
      sp_labels.reserve(num_nodes+2);

      for( long int t_now=0;t_now<max_t;t_now++)
      {
         for(long int site=0;site<N;site++)
         {
            std::array<long int,2> edge_now;
            edge_now[0] = t_now*N+site;
            edge_now[1] = t_now*N+(site+1)%N;
            edges.push_back(edge_now);

            edge_now[0] = t_now*N+site;
            edge_now[1] = t_now*N+site+N;
            edges.push_back(edge_now);
         }
      }

      if(edges.size() != num_edges )
      {
         std::cerr << "number of total edges don't match" << std::endl;
         std::cerr << "There supposed to be " << num_edges << " edges" << std::endl;
         std::cerr << "Number of edges " << edges.size() << std::endl;
         std::exit(1);
      }

      for(long int node=0;node<(num_nodes); node++)
      {
         labels.push_back(node);
      }

      edges_orig=edges;
      labels_orig=labels;
      cluster_sizes=std::vector<long int>(num_nodes,1);

      sp_labels=labels;
      for(long int node=0;node<2;node++)
      {
         sp_labels.push_back(node);
      }

      sp_labels[num_nodes+0]=num_nodes+0;
      for(long int node=0;node<num_aux_nodes0;node++)
      {
         sp_edges.push_back(std::array<long int,2> {node,num_nodes+0});
      }

      sp_labels[num_nodes+1]=num_nodes+1;
      for(long int node=0;node<num_aux_nodes1;node++)
      {
         sp_edges.push_back(std::array<long int,2> {num_nodes-(num_aux_nodes1)+node,num_nodes+1});
      }

      sp_labels_orig=sp_labels;
      sp_cluster_sizes=std::vector<long int>(num_nodes+2,1);

      for(auto sp_edge=sp_edges.begin(); sp_edge != sp_edges.end(); sp_edge++)
      {
         quick_union(sp_labels,sp_cluster_sizes,*sp_edge);
      }

      std::cout <<"Lattice constructed" << std::endl;
      constructed=true;
   }


   void CPercolate::construct_network_PWR2()
   {
      if( constructed )
      {
         std::cerr << "Lattice already constructed" << std::endl;
         std::exit(1);
      }

      num_edges=2*(N/2)*(max_t);
      num_nodes=(N/2)*(max_t+1);

      num_aux_nodes0=N/2;
      num_aux_nodes1=N/2;

      edges.reserve(num_edges);
      labels.reserve(num_nodes);
      sp_labels.reserve(num_nodes+2);

      int dmax=int_log2(N)-1;
      int block_num_layers=2*int_log2(N)-1;
      int half_num_block_layers=int_log2(N)-1;

      long int num_edges_block=2*(block_num_layers)*(N/2);
      long int num_nodes_block=(N/2)*(block_num_layers+1);

      std::vector<std::array<long int,2>> edges_block;
      edges_block.reserve(num_edges_block);

      long int node_block_now=0;
      for( long int t_now=0;t_now<block_num_layers;t_now++)
      {
         std::vector<std::array<long int,2>> edges_now;
         std::vector<std::array<long int,2>> edges_next;

         unsigned long int d=1;
         unsigned long int addfactor=0;

         if ((t_now%block_num_layers)==half_num_block_layers)
         {
            d=std::pow(2,int_log2(N)-1);
            addfactor=0;

            unsigned int edges_now_ind=0;
            for( long int site=0; site  < (N/2) ; site++)
            {
               std::array<long int,2> edge_now;
               edge_now[0]=site;
               edge_now[1]=(edge_now[0]+d)%N;
               edges_now.push_back(edge_now);
               edges_now_ind+=1;
            }
         }
         else
         {
            d=std::pow(2,t_now%block_num_layers);
            addfactor=0;
            if (t_now%block_num_layers > (half_num_block_layers-1))
            {
               d=std::pow(2,t_now%block_num_layers-half_num_block_layers-1);
               addfactor=d;
            }

            unsigned int edges_now_ind=0;
            for( long int site0=0; site0  < d ; site0 ++)
            {
               for(long int site=(site0+addfactor); site<N; site+=2*d)
               {
                  std::array<long int,2> edge_now;
                  edge_now[0]=site;
                  edge_now[1]=(edge_now[0]+d)%N;
                  edges_now.push_back(edge_now);
                  edges_now_ind+=1;
               }
            }
         }

         std::cout << "t=" << t_now << " d=" << d << std::endl;

         if (((t_now+1)%block_num_layers)==half_num_block_layers)
         {
            d=std::pow(2,int_log2(N)-1);
            addfactor=0;

            unsigned int edges_next_ind=0;
            for( long int site=0; site  < (N/2) ; site++)
            {
               std::array<long int,2> edge_now;
               edge_now[0]=site;
               edge_now[1]=(edge_now[0]+d)%N;

               // std::cout << "next " << edge_now[0] << "," << edge_now[1] << std::endl;
               edges_next.push_back(edge_now);
               edges_next_ind+=1;
            }
         }
         else
         {
            d=std::pow(2,(t_now+1)%block_num_layers);
            addfactor=0;
            if ((t_now+1)%block_num_layers > (half_num_block_layers-1))
            {
               d=std::pow(2,(t_now+1)%block_num_layers-half_num_block_layers-1);
               addfactor=d;
            }

            unsigned int edges_next_ind=0;
            for( long int site0=0; site0  < d ; site0 ++)
            {
               for(long int site=(site0+addfactor); site<N; site+=2*d)
               {
                  std::array<long int,2> edge_now;
                  edge_now[0]=site;
                  edge_now[1]=(edge_now[0]+d)%N;

                  // std::cout << "next " << edge_now[0] << "," << edge_now[1] << std::endl;
                  edges_next.push_back(edge_now);
                  edges_next_ind+=1;
               }
            }
         }

         std::cout << "t=" << t_now+ 1 << " d=" << d << std::endl;
         if (edges_now.size() != N/2)
         {
            std::cerr << "Number of sites and number of edges don't match" << std::endl;
            std::cerr << "N/2=" << N/2 << std::endl;
            std::cerr << "Number of edges" << edges_now.size() << std::endl;
            std::exit(1);
         }

         if (edges_next.size() != N/2)
         {
            std::cerr << "Number of sites and number of edges don't match" << std::endl;
            std::cerr << "N/2=" << N/2 << std::endl;
            std::cerr << "Number of edges" << edges_now.size() << std::endl;
            std::exit(1);
         }

         for( auto it_now=edges_now.begin() ; it_now !=edges_now.end(); it_now++)
         {
            // std::cout << (*it_now)[0]  << "," << (*it_now)[1] << std::endl;
            //
            std::array<long int,2> connected_nodes;
            connected_nodes=find_next(*it_now,edges_next);
            connected_nodes[0]+= (t_now+1)*(N/2);
            connected_nodes[1]+= (t_now+1)*(N/2);

            // std::cout << node_block_now << "," << connected_nodes[0] << std::endl;
            // std::cout << node_block_now << "," << connected_nodes[1] << std::endl;

            edges_block.push_back( std::array<long int,2>{node_block_now, connected_nodes[0]} );
            edges_block.push_back( std::array<long int,2>{node_block_now, connected_nodes[1]} );
            node_block_now+=1;
         }
      }

      if (edges_block.size() != num_edges_block )
      {
         std::cerr << "number of edges per block don't match" << std::endl;
         std::cerr << "There supposed to be " << 2*(block_num_layers*N/2) << " edges" << std::endl;
         std::cerr << "Number of edges" << edges_block.size() << std::endl;
         std::exit(1);
      }

      for( long int t_now=0;t_now<max_t;t_now++)
      {
         long int offset_node = t_now/block_num_layers;
         long int offset_block = t_now%block_num_layers;

         for(long int ind=0; ind<2*(N/2); ind++)
         {
            std::array<long int,2> edge_now;
            edge_now=edges_block[ind+2*offset_block*(N/2)];
            edge_now[0]+= block_num_layers*offset_node*(N/2);
            edge_now[1]+= block_num_layers*offset_node*(N/2);
            edges.push_back(edge_now);
         }
      }

      if(edges.size() != num_edges )
      {
         std::cerr << "number of total edges don't match" << std::endl;
         std::cerr << "There supposed to be " << num_edges << " edges" << std::endl;
         std::cerr << "Number of edges " << edges.size() << std::endl;
         std::exit(1);
      }

      for(long int node=0;node<(num_nodes); node++)
      {
         labels.push_back(node);
      }

      edges_orig=edges;
      labels_orig=labels;
      cluster_sizes=std::vector<long int>(num_nodes,1);

      sp_labels=labels;
      for(long int node=0;node<2;node++)
      {
         sp_labels.push_back(node);
      }
      sp_labels[num_nodes+0]=num_nodes+0;

      for(long int node=0;node<num_aux_nodes0;node++)
      {
         sp_edges.push_back(std::array<long int,2> {node,num_nodes+0});
      }
      sp_labels[num_nodes+1]=num_nodes+1;

      sp_labels_orig=sp_labels;
      sp_cluster_sizes=std::vector<long int>(num_nodes+2,1);

      for(long int node=0;node<num_aux_nodes1;node++)
      {
         sp_edges.push_back(std::array<long int,2> {num_nodes-(num_aux_nodes1)+node,num_nodes+1});
      }

      for(auto sp_edge=sp_edges.begin(); sp_edge != sp_edges.end(); sp_edge++)
      {
         quick_union(sp_labels,sp_cluster_sizes,*sp_edge);
      }

      std::cout <<"Lattice constructed" << std::endl;
      constructed=true;
   }

   void CPercolate::construct_network_PWR2_depth(int depth)
   {
      if( constructed )
      {
         std::cerr << "Lattice already constructed" << std::endl;
         std::exit(1);
      }

      if ( depth >= int_log2(N) ){
         this -> construct_network_PWR2();
         return;
      }

      num_edges=2*(N/2)*(max_t);
      num_nodes=(N/2)*(max_t+1);

      num_aux_nodes0=N/2;
      num_aux_nodes1=N/2;

      edges.reserve(num_edges);
      labels.reserve(num_nodes);
      sp_labels.reserve(num_nodes+2);

      int dmax=depth;
      int block_num_layers=2*depth;
      int half_num_block_layers=depth;

      long int num_edges_block=2*(block_num_layers)*(N/2);
      long int num_nodes_block=(N/2)*(block_num_layers+1);

      std::vector<std::array<long int,2>> edges_block;
      edges_block.reserve(num_edges_block);

      long int node_block_now=0;
      for( long int t_now=0;t_now<block_num_layers;t_now++)
      {
         std::vector<std::array<long int,2>> edges_now;
         std::vector<std::array<long int,2>> edges_next;

         unsigned long int d=1;
         unsigned long int addfactor=0;

         d=std::pow(2,t_now%block_num_layers);
         addfactor=0;
         if (t_now%block_num_layers >= (half_num_block_layers) )
         {
            d=std::pow(2,t_now%block_num_layers-half_num_block_layers);
            addfactor=d;
         }

         unsigned int edges_now_ind=0;
         for( long int site0=0; site0  < d ; site0 ++)
         {
            for(long int site=(site0+addfactor); site<N; site+=2*d)
            {
               std::array<long int,2> edge_now;
               edge_now[0]=site;
               edge_now[1]=(edge_now[0]+d)%N;
               edges_now.push_back(edge_now);
               edges_now_ind+=1;
            }
         }

         d=std::pow(2,(t_now+1)%block_num_layers);
         addfactor=0;
         if ( ((t_now+1)%block_num_layers) >= (half_num_block_layers) )
         {
            d=std::pow(2,(t_now+1)%block_num_layers-half_num_block_layers);
            addfactor=d;
         }

         unsigned int edges_next_ind=0;
         for( long int site0=0; site0  < d ; site0 ++)
         {
            for(long int site=(site0+addfactor); site<N; site+=2*d)
            {
               std::array<long int,2> edge_now;
               edge_now[0]=site;
               edge_now[1]=(edge_now[0]+d)%N;

               // std::cout << "next " << edge_now[0] << "," << edge_now[1] << std::endl;
               edges_next.push_back(edge_now);
               edges_next_ind+=1;
            }
         }

         if (edges_now.size() != N/2)
         {
            std::cerr << "Number of sites and number of edges don't match" << std::endl;
            std::cerr << "N/2=" << N/2 << std::endl;
            std::cerr << "Number of edges" << edges_now.size() << std::endl;
            std::exit(1);
         }

         if (edges_next.size() != N/2)
         {
            std::cerr << "Number of sites and number of edges don't match" << std::endl;
            std::cerr << "N/2=" << N/2 << std::endl;
            std::cerr << "Number of edges" << edges_now.size() << std::endl;
            std::exit(1);
         }

         for( auto it_now=edges_now.begin() ; it_now !=edges_now.end(); it_now++)
         {
            // std::cout << (*it_now)[0]  << "," << (*it_now)[1] << std::endl;
            //
            std::array<long int,2> connected_nodes;
            connected_nodes=find_next(*it_now,edges_next);
            connected_nodes[0]+= (t_now+1)*(N/2);
            connected_nodes[1]+= (t_now+1)*(N/2);

            // std::cout << node_block_now << "," << connected_nodes[0] << std::endl;
            // std::cout << node_block_now << "," << connected_nodes[1] << std::endl;

            edges_block.push_back( std::array<long int,2>{node_block_now, connected_nodes[0]} );
            edges_block.push_back( std::array<long int,2>{node_block_now, connected_nodes[1]} );
            node_block_now+=1;
         }
      }

      if (edges_block.size() != num_edges_block )
      {
         std::cerr << "number of edges per block don't match" << std::endl;
         std::cerr << "There supposed to be " << 2*(block_num_layers*N/2) << " edges" << std::endl;
         std::cerr << "Number of edges" << edges_block.size() << std::endl;
         std::exit(1);
      }

      for( long int t_now=0;t_now<max_t;t_now++)
      {
         long int offset_node = t_now/block_num_layers;
         long int offset_block = t_now%block_num_layers;

         for(long int ind=0; ind<2*(N/2); ind++)
         {
            std::array<long int,2> edge_now;
            edge_now=edges_block[ind+2*offset_block*(N/2)];
            edge_now[0]+= block_num_layers*offset_node*(N/2);
            edge_now[1]+= block_num_layers*offset_node*(N/2);
            edges.push_back(edge_now);
         }
      }

      if(edges.size() != num_edges )
      {
         std::cerr << "number of total edges don't match" << std::endl;
         std::cerr << "There supposed to be " << num_edges << " edges" << std::endl;
         std::cerr << "Number of edges " << edges.size() << std::endl;
         std::exit(1);
      }

      for(long int node=0;node<(num_nodes); node++)
      {
         labels.push_back(node);
      }

      edges_orig=edges;
      labels_orig=labels;
      cluster_sizes=std::vector<long int>(num_nodes,1);

      sp_labels=labels;
      for(long int node=0;node<2;node++)
      {
         sp_labels.push_back(node);
      }
      sp_labels[num_nodes+0]=num_nodes+0;

      for(long int node=0;node<num_aux_nodes0;node++)
      {
         sp_edges.push_back(std::array<long int,2> {node,num_nodes+0});
      }
      sp_labels[num_nodes+1]=num_nodes+1;

      sp_labels_orig=sp_labels;
      sp_cluster_sizes=std::vector<long int>(num_nodes+2,1);

      for(long int node=0;node<num_aux_nodes1;node++)
      {
         sp_edges.push_back(std::array<long int,2> {num_nodes-(num_aux_nodes1)+node,num_nodes+1});
      }

      for(auto sp_edge=sp_edges.begin(); sp_edge != sp_edges.end(); sp_edge++)
      {
         quick_union(sp_labels,sp_cluster_sizes,*sp_edge);
      }

      std::cout <<"Lattice constructed" << std::endl;
      constructed=true;
   }

   void CPercolate::construct_network_AA()
   {
      if( constructed )
      {
         std::cerr << "Lattice already constructed" << std::endl;

         edges.erase(edges.begin(),edges.end());
         sp_edges.erase(sp_edges.begin(),sp_edges.end());

         labels.erase(labels.begin(),labels.end());
         sp_labels.erase(sp_labels.begin(),sp_labels.end());
      }

      num_edges=2*(N/2)*(max_t);
      num_nodes=(N/2)*(max_t+1);

      num_aux_nodes0=N/2;
      num_aux_nodes1=N/2;

      edges.reserve(num_edges);
      labels.reserve(num_nodes);
      sp_labels.reserve(num_nodes+2);

      std::vector<long int> sites_vec;
      for(long int ind=0;ind<N;ind++) sites_vec.push_back(ind); 

      std::vector<long int> sites_now=sites_vec;
      std::random_shuffle(sites_now.begin(),sites_now.end());
      std::vector<long int> sites_next;

      for( long int t_now=0;t_now<max_t;t_now++)
      {

         std::vector<std::array<long int,2>> edges_now;
         std::vector<std::array<long int,2>> edges_next;

         for(long int site=0; site<N; site+=2)
         {
            std::array<long int,2> edge_now;
            edge_now[0]=sites_now[site];
            edge_now[1]=sites_now[(site+1)%N];
            edges_now.push_back(edge_now);
         }
         
         sites_next=sites_vec;
         std::random_shuffle(sites_next.begin(),sites_next.end());

         // for(auto it=sites_next.begin(); it!=sites_next.end();it++) std::cout << (*it) << ",";
         // std::cout << std::endl;

         for(long int site=0; site<N; site+=2)
         {
            std::array<long int,2> edge_now;
            edge_now[0]=sites_next[site];
            edge_now[1]=sites_next[(site+1)%N];
            edges_next.push_back(edge_now);
         }

         long int node_now=0;
         for( auto it_now=edges_now.begin() ; it_now !=edges_now.end(); it_now++)
         {
            std::array<long int,2> connected_node;
            connected_node=find_next(*it_now,edges_next);

            if (( connected_node[0] < 0 ) || ( connected_node[1] < 0 ) )
            {
               std::cout<< connected_node[0] << "," << connected_node[1]  << std::endl;
            }

            if ( connected_node[0] >= 0 ) 
            {
               connected_node[0]+= (t_now+1)*(N/2);
               edges.push_back( std::array<long int,2>{node_now+(t_now)*(N/2), connected_node[0]} );
            }

            if ( connected_node[1] >= 0 ) 
            {
               connected_node[1]+= (t_now+1)*(N/2);
               edges.push_back( std::array<long int,2>{node_now+(t_now)*(N/2), connected_node[1]} );
            }
            node_now+=1;
         }

         sites_now=sites_next;
      }

      if(edges.size() != num_edges )
      {
         std::cerr << "the total number of edges don't match" << std::endl;
         std::cerr << "There supposed to be " << num_edges << " edges" << std::endl;
         std::cerr << "Number of edges " << edges.size() << std::endl;
      }

      for(long int node=0;node<(num_nodes); node++)
      {
         labels.push_back(node);
      }

      num_edges=edges.size();
      edges_orig=edges;
      labels_orig=labels;
      cluster_sizes=std::vector<long int>(num_nodes,1);

      sp_labels=labels;
      for(long int node=0;node<2;node++)
      {
         sp_labels.push_back(node);
      }

      sp_labels[num_nodes+0]=num_nodes+0;
      for(long int node=0;node<num_aux_nodes0;node++)
      {
         sp_edges.push_back(std::array<long int,2> {node,num_nodes+0});
      }

      sp_labels[num_nodes+1]=num_nodes+1;
      sp_labels_orig=sp_labels;
      sp_cluster_sizes=std::vector<long int>(num_nodes+2,1);

      for(long int node=0;node<num_aux_nodes1;node++)
      {
         sp_edges.push_back(std::array<long int,2> {num_nodes-(num_aux_nodes1)+node,num_nodes+1});
      }

      for(auto sp_edge=sp_edges.begin(); sp_edge != sp_edges.end(); sp_edge++)
      {
         quick_union(sp_labels,sp_cluster_sizes,*sp_edge);
      }

      std::cout <<"Lattice constructed" << std::endl;
      constructed=true;
   }
}
