import numpy as np

# This is a preliminary python implementation for analyszing the results of Newman-Ziff algorithm.
# It implements equation (2) and (4) for observable calculation.
# Please cite: 
# Newman, M. E. J., & Ziff, R. M. (2001). Fast Monte Carlo algorithm for site or bond percolation. 
# In Physical Review E (Vol. 64, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physreve.64.016706.

def log_coeff(N,n) :
   if n==0 :
      return 0
   if n==N :
      return 0
   coeff=np.log2(np.arange(N-n+1,N+1)).sum()-np.log2(np.arange(1,n+1)).sum()
   #print(coeff)
   return coeff

def make_logbncoeff(N) :
   return np.array(list(map(log_coeff,[N]*N,np.arange(1,N+1))))

def get_BDavg(pp,Qn,NN) :
   BNnp=np.zeros(len(Qn))
   nmax=int(pp*NN)
   BNnp[nmax-1]=1

   for n in range(nmax+1,len(Qn)+1) :
      BNnp[n-1]=BNnp[n-1-1]*((NN-n+1)/n)*(pp/(1.-pp))

   for n in range(nmax-1,0,-1) :
      BNnp[n-1]=BNnp[n]*((n+1)/(NN-n))*((1-pp)/pp)

   return np.sum(BNnp*Qn)/np.sum(BNnp)

def get_BDerr(pp,Qn,NN) :
   BNnp=np.zeros(len(Qn))
   nmax=int(pp*NN)
   BNnp[nmax-1]=1

   for n in range(nmax+1,len(Qn)+1) :
      BNnp[n-1]=BNnp[n-1-1]*((NN-n+1)/n)*(pp/(1.-pp))
   for n in range(nmax-1,0,-1) :
      BNnp[n-1]=BNnp[n]*((n+1)/(NN-n))*((1-pp)/pp)

   return np.sqrt(np.sum(BNnp*BNnp*Qn*Qn)/(np.sum(BNnp)*np.sum(BNnp)))
