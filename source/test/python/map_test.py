import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import sys,os

import Percolation_Mapping as PM

# This is for debugging the percolation mapping algorithm of Haar-random circuit.
# e.g. Fig 4 from
# Skinner, B., Ruhman, J., & Nahum, A. (2019). Measurement-Induced Phase Transitions in the Dynamics of Entanglement. 
# In Physical Review X (Vol. 9, Issue 3). American Physical Society (APS). https://doi.org/10.1103/physrevx.9.031009. 
# and Sect. III from 
# Hashizume, T., Bentsen, G., & Daley, A. J. (2022). Measurement-induced phase transitions in sparse nonlocal scramblers. 
# In Physical Review Research (Vol. 4, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physrevresearch.4.013174.

def pwr2(Length,ss_exp,tmax) :
   gate_list=[]
   for t in range(1,tmax+1) :
      tot_weight=0
      tot_weight+=(Length/2.)**ss_exp
      for temp_i in range(int(np.log2(Length)-1)) :
         d=2.**temp_i;
         tot_weight += 2*(d**ss_exp);

      normfac=tot_weight/2.;

      for log2d in range(int(np.log2(Length)-1)) :
         d=2.**log2d
         prob = (d**ss_exp)/normfac

         for site0 in range(int(d)) :
            for site in range(int(site0+d*(t%2)),int(Length),int(2*d)) :
               site1=site;
               site2=(site1+d)%(Length);
               if np.random.uniform() < prob :
                  gate_list.append([int(site1),int(site2)])

      if(t%2 == 0) :
         log2d=int(np.log2(Length)-1);
         d=(2.**log2d);
         prob = (d**ss_exp)/normfac; 

         for site in range(int(Length/2)) :
            site1=site;
            site2=(site1+d)%(Length);
            if np.random.uniform() < prob :
               gate_list.append([int(site1),int(site2)])

   return gate_list

verbose=False 
ns=[6,7,8,9]
ss=[-8,-5,-3,-2,-1,-0.5,-0.4,-0.3,-0.2,0]
tmax=2
numtraj=100

radius_list=[]
radiuss_list_e=[]

for n in ns :
   Length=int(2**n)
   plot=True

   radiuss=[]
   radiuss_e=[]
   if Length > 50:
      plot=False
   outfname='radius_on_nodes_{}.out'.format(Length)
   if os.path.exists(outfname) : 
      [tmp,radiuss,radiuss_e]=np.loadtxt(outfname)
   else :
      for s in ss :
         radiuss_curr=[]
         radiuss_e_curr=[]
         for traj in range(numtraj) :
            gate_list=pwr2(Length,s,tmax)
            nodes,edges=PM.make_graph(gate_list,Length)

            if verbose :
               print(gate_list)
               print(len(nodes))
               print(max(nodes))
               print(len(edges)+Length)

            G=nx.Graph()
            for node in nodes :
               G.add_node(node)

            for edge in edges :
               G.add_edge(edge[0],edge[1])

            G=G.subgraph(list(range(Length,max(nodes)-Length)))
            subgraphs = nx.connected_components(G)

            am=np.argmax(map(lambda x: len(x),subgraphs))
            SG_now=G.subgraph(list(subgraphs)[am])
            rr=nx.radius(SG_now)/len(SG_now)
            radiuss_curr.append(rr)

            #for SG in subgraphs :
               #SG_now=G.subgraph(SG)
               #print('s=',s,'r=',nx.radius(SG_now),',',len(SG),'nodes',',',SG_now.number_of_edges(),'edges')

         radiuss.append(np.mean(radiuss_curr))
         radiuss_e.append(np.std(radiuss_curr)/np.sqrt(len(radiuss_curr)))

         print('N=',Length,'s=',s,'r=',radiuss[-1],flush=True)
         if plot :
            highlight_nodes = list(range(0,Length)) + list(range(max(nodes)-Length,max(nodes)))
            colors = ['red' if node_name in highlight_nodes else 'blue' for node_name in list(G.nodes)]

            options = {
               'node_color': 'black',
               'node_size': 100,
               'width': 3,
               'node_color': colors,
            }

            print('is_tree?',nx.is_tree(G))

            plt.figure()
            plt.title("$s={}$".format(s))
            nx.draw(G,**options)
      np.savetxt(outfname,[ss,radiuss,radiuss_e])
   radius_list.append(radiuss.copy())
   radiuss_list_e.append(radiuss_e.copy())

plt.figure()
plt.title('Graph Radius/number of nodes')
for ind,radiuss in enumerate(radius_list) :
   plt.errorbar(ss,radiuss,radiuss_list_e[ind],label="$N={}$".format(2**ns[ind]))

plt.ylim(0,0.6)
plt.xlabel('$s$')
plt.legend()
#plt.savefig('Radius_on_nodes_pwr2.png',dpi=300)
plt.show()
