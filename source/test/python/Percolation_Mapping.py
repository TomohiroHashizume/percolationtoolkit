import numpy as np

# This is a class that does percolation mapping of two-qubit Haar-random circuit.
# e.g. Fig 4 from
# Skinner, B., Ruhman, J., & Nahum, A. (2019). Measurement-Induced Phase Transitions in the Dynamics of Entanglement. 
# In Physical Review X (Vol. 9, Issue 3). American Physical Society (APS). https://doi.org/10.1103/physrevx.9.031009. 
# and Sect. III from 
# Hashizume, T., Bentsen, G., & Daley, A. J. (2022). Measurement-induced phase transitions in sparse nonlocal scramblers. 
# In Physical Review Research (Vol. 4, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physrevresearch.4.013174.

def flatten_nodes(nodes):
   out=[]
   for node_layer in nodes :
      for site in node_layer:
         out.append(site)
   return out

def find_edge(nodes,node_sites,site,nodenow) :
   ind=len(node_sites)
   while ind > 0 :
      ind-=1
      for nsind,node_site in enumerate(node_sites[ind]) :
         if site in node_site :
            node_connect=nodes[ind][nsind]
            return [nodenow,node_connect]
   return None

def find_edges(nodes,node_sites,gate,nodenow) :
   out=[]
   for site in gate :
      out.append(find_edge(nodes,node_sites,site,nodenow))
   return out

def make_graph(gatelist,N) :
   nodes=[list(np.arange(N,dtype=int))]
   node_sites=[list(map(lambda x : [x],list(np.arange(N,dtype=int))))]#real sites that a 2site gate connects

   layernow=1
   nodenow=N
   sites=list(np.arange(N,dtype=int))

   edge_list=[]
   append_new=False

   for gate in gatelist : #must be 2 qubit gate
      if (not (gate[0] in sites)) or (not (gate[1] in sites)):
         layernow+=1
         sites=list(np.arange(N,dtype=int))
         nodes.append([])
         node_sites.append([])
      else :
         if len(nodes) < (layernow+1):
            nodes.append([])
            node_sites.append([])

      edges=find_edges(nodes,node_sites,gate,nodenow)

      nodes[layernow].append(nodenow)
      node_sites[layernow].append((gate.copy())) #type of gate must be list

      for edge in edges :
         edge_list.append(edge.copy())

      nodenow+=1
      sites.remove(int(gate[0]))
      sites.remove(int(gate[1]))

   layernow+=1
   sites=list(np.arange(N,dtype=int))
   nodes.append([])
   node_sites.append([])

   for site in sites :
      edge_list.append(find_edge(nodes,node_sites,site,nodenow))
      nodes[layernow].append(nodenow)
      node_sites[layernow].append([site])
      nodenow+=1

   nodes=flatten_nodes(nodes)
   return nodes,edge_list
