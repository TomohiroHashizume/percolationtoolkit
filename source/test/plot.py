import numpy as np
import matplotlib.pyplot as plt

N=256
N=512
N=1024
N=2048
N=4096

mindp=0.01
maxdp=0.1

n=0
nmax=100

count=0
chi=np.zeros(N*N)

for i in range(n,n+nmax) :
   try : 
      chi+=abs(np.loadtxt("chi_"+str(N)+"_"+str(i)+".out"));
      count+=1;
      print(count)
   except :
      pass

chi=chi/count;
p=np.arange(len(chi))/len(chi)
plt.figure()
plt.plot(p,chi)
plt.yscale('log')

plt.figure()
chi_max_ind=np.argmax(chi)
chi=chi[p<p[chi_max_ind]]
p=p[p<p[chi_max_ind]]

print(p[np.argmax(chi)])

plt.plot(0.5-p,chi)

pbar=0.5-p

chi=chi[pbar>mindp]
pbar=pbar[pbar>mindp]

chi=chi[pbar<maxdp]
pbar=pbar[pbar<maxdp]

print(np.polyfit(np.log(pbar),np.log(chi),1))
plt.yscale('log')
plt.xscale('log')
plt.show()
