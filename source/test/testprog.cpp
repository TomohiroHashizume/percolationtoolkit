#include <string>
#include "NZ.h"

int main(int argc,char *argv[])
{
   std::random_device rnd;
   std::mt19937 mt(rnd());

   long int n=std::atoi(argv[1]);
   long int N=std::pow(2,n);

   // network.construct_network_PWR2();
   // network.construct_network_AA();

   for(long int i=0;i<100;i++)
   {
      std::string destdir="./data/NN/"+std::to_string(N)+"/";
      CPercolate::CPercolate network(N,N);
      network.construct_network_AA();
      network.NZ(i+1);
      network.output_obs(destdir);
      network.erase_obs();
   }
}
