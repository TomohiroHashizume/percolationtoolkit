#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <random>
#include <set>
#include <stdio.h>
#include <string>

// This is a utility for analyzing the result of 
// Newman-Ziff algorithm.
// Please cite: 
// Newman, M. E. J., & Ziff, R. M. (2001). Fast Monte Carlo algorithm for site or bond percolation. 
// In Physical Review E (Vol. 64, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physreve.64.016706.

namespace CPercolate {
   int int_log2(int num)
   {
      int count=0;
      while(num > 1)
      {
         num /= 2;
         count++;
      }
      return count;
   }

   std::vector<double> make_BNnp(double pp,long int NN)
   {
      std::vector<double> BNnp(NN,0.0);
      int nmax=int(pp*NN);
      BNnp[nmax]=1;

      for( int n=nmax-1; n >= 0; n--)
      {
         BNnp[n]=BNnp[n+1]*((n+1)/(NN-n))*((1-pp)/pp);
      }

      for( int n=nmax+1; n<NN+1; n++){
         BNnp[n]=BNnp[n-1]*((NN-n+1)/n)*(pp/(1.-pp));
      }

      return BNnp;
   }


   template<typename NumType>
   bool write_binary(std::string filepath, std::vector<NumType> &vec)
   {
    std::ofstream fout(filepath, std::ios::out | std::ios::binary);
    fout.write((char*)&vec[0], vec.size()*sizeof(vec[0]));
    fout.close();
    return true;
   }

   template<typename NumType>
   bool read_binary(std::string filepath, std::vector<NumType> &vec){
       std::ifstream fin(filepath, std::ios::in | std::ios::binary);
       fin.read((char*)&vec[0], vec.size()*sizeof(vec[0]));
       fin.close();
       return true;
   }

   template<typename T>
   void print_obs_to_file(std::string fname, std::vector<T> &obs_vec)
   {
      std::ofstream outfile;
      outfile.open(fname);
      for(int i=0; i<obs_vec.size(); i++)
      {
         outfile << obs_vec[i] << std::endl;
      }
      outfile.close();
   }

   template<typename NumType1, typename NumType2>
   std::vector<double> add(std::vector<NumType1> &vec0,std::vector<NumType2> &vec1,double factor=1)
   {
      if(vec0.size() != vec1.size())
      {
         std::cout <<"Addition went wrong" <<std::endl;
         throw 0;
      }

      std::vector<double> out;
      for(int ind=0;ind<vec0.size();ind++)
      {
         out.push_back(double(vec0[ind])+factor*double(vec1[ind]));
      }
      return out;
   }

   template<typename NumType1, typename NumType2>
   std::vector<double> mult(std::vector<NumType1> &vec0,std::vector<NumType2> &vec1,NumType1 factor=-1)
   {
      if(vec0.size() != vec1.size())
      {
         std::cout <<"mult: vec0.size() != vec1.size() " <<std::endl;
         throw 0;
      }

      std::vector<double> out;
      for(int ind=0;ind<vec0.size();ind++)
      {
         out.push_back(double(vec0[ind])*double(vec1[ind]));
      }

      if(out.size() != vec0.size())
      {
         std::cout <<"mult: out.size() != vec0.size() " <<std::endl;
         throw 0;
      }
      
      return out;
   }

   template<typename NumType>
   std::vector<double> mult(std::vector<NumType> &vec,double factor)
   {
      std::vector<double> out;
      for(int ind=0;ind<vec.size();ind++)
      {
         out.push_back(vec[ind]*factor);
      }
      return out;
   }
}
