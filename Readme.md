# Percolation Toolkit 

The software implements Newman-Ziff algorithm for finding critical point of non-weighted lattice. 

This is especially useful to map Haar-random quantum circuit to a network and calculate critical points of measurement-induced phase transition (c.f. Hashizume et al. (2022)).

## Cite 
Please cite the following:

The software is based on:
* M. E. J. Newman, R. M. Ziff. (2001). [Phys. Rev. E 64, 016706](https://doi.org/10.1103/PhysRevE.64.016706). 

Efficient Find-Union algorithm is based on:
* J. E. Hopcroft, J. D. Ullman (1973). [SIAM Journal on Computing 2, 294](https://doi.org/10.1137/0202024).
* R. E. Tarjan. (1975). [Journal of the ACM (JACM) 22, 215](https://doi.org/10.1145/321879.321884).

Cite this software as:
* Tomohiro Hashizume. (2022). Percolation Toolkit 0.1. [https://doi.org/10.5281/zenodo.7245837](https://doi.org/10.5281/zenodo.7245837).

Results obtained from this his and older versions of the software are Published in:
* Tomohiro Hashizume, Gregory Bentsen, Andrew J. Daley. (2022). [Phys. Rev. Research 4, 013174](https://doi.org/10.1103/PhysRevResearch.4.013174).

## TODO

Complete this read me...

## Usage

Please see examples in `test/source`.

## Contact

* Tomohiro Hashizume (original developer) tomohiro.hashizume.overseas@gmail.com

## License

MIT License

Copyright (c) [2022] [Tomohiro Hashizume]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
Ongoing...
